package JobTest;

public class Products {

	private String name;
	private double price;
	private double reviews;
	
	public Products(String name, double price, double reviews) {
		this.name = name;
		this.price = price;
		this.reviews = reviews;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public double getReviews() {
		return reviews;
	}
	
	public String toString() {
		return name+ "\n" + "price : "+price+ "\n" + "review : " + reviews; 	
	}
}
