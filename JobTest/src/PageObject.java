package JobTest;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageObject {

	WebDriver driver = null;
	OperationMethods om;
	private static WebElement element = null;

	public PageObject(WebDriver driver) {
		this.driver = driver;
	}

	// Homepage search box
	public WebElement searchBox() {
		element = driver.findElement(By.cssSelector("#twotabsearchtextbox"));
		return element;
	}

	// Refine search by $ Low field
	public WebElement lowPrice() {
		element = driver.findElement(By.cssSelector("#low-price"));
		return element;
	}

	// Refine search by $ High field
	public WebElement highPrice() {
		element = driver.findElement(By.cssSelector("#high-price"));
		return element;
	}

	// Refine search by $ "GO" button (DYNAMIC ID)
	public WebElement goButton() {
		element = driver.findElement(By.xpath(".//*[@method='get']/span[contains(.,'Go')]"));
		return element;
	}

	// Case Material - Plastic Option (DYNAMIC)
	public WebElement plasticCheckbox() {
		element = driver.findElement(By.xpath(
				".//*[@id='leftNavContainer']/ul[contains(.,'Plastic')]/div/li[contains(.,'Plastic')]/span/span/div/label/input"));
		return element;
	}

	// Submitting an item to search for
	public void searchForItem(String item) {
		element = searchBox();
		element.sendKeys(item);
		element.sendKeys(Keys.ENTER);
	}

	// Clicking the Plastic checkbox option if it hasnt already been selected
	public void clickPlastic() {
		element = plasticCheckbox();
		if (element.getAttribute("value").contains("false")) {
			element.click();
		}
	}

	// Refining the search parameters by a defined amount
	public void refineBy$(String lowAmount, String highAmount) {
		lowPrice().sendKeys(lowAmount);
		highPrice().sendKeys(highAmount);
		goButton().click();
	}

}