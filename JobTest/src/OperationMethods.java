package JobTest;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OperationMethods {
	
	WebDriver driver = null;
	private static WebElement element = null;
	private static String text1 = null;
	
	public OperationMethods(WebDriver driver) {
		this.driver = driver;
	}

	// Create element list to check if present
	public List<WebElement> elmList(String type, String locator) {
		type = type.toLowerCase();
		if (type.equals("id")) {
			return driver.findElements(By.id(locator));
		} else if (type.equals("xpath")) {
			return driver.findElements(By.xpath(locator));
		} else if (type.equals("css")) {
			return driver.findElements(By.cssSelector(locator));
		} else {
			return null;
		}
	}
	
	// Check if element is present
	public boolean isPresent(String type, String locator) {
		List<WebElement> elementList = elmList(type, locator);
		if (elementList.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// Return total price from split multi element
	public double returnPrice (String text1, String text2) {
		String priceText = text1 + "." + text2;
		double price = Double.parseDouble(priceText);
		return price;
	}
	
	// Returning 1st result name (DYNAMIC - [@id='result_x'])
		public String firstResultName() {
			element = driver.findElement(By.xpath(".//*[@id='result_0']/div/div[3]/div[1]/a/h2"));
			text1 = element.getText();
			return text1;
		}

		// Returning 2nd result name (DYNAMIC - [@id='result_x'])
		public String secondResultname() {
			element = driver.findElement(By.xpath(".//*[@id='result_1']/div/div[3]/div[1]/a/h2"));
			text1 = element.getText();
			return text1;
		}
		
		// Returning 3nd result name (DYNAMIC - [@id='result_x'])
		public String thridResultname() {
			element = driver.findElement(By.xpath(".//*[@id='result_2']/div/div[3]/div[1]/a/h2"));
			text1 = element.getText();
			return text1;
		}
		
		// Returning 4th result name (DYNAMIC - [@id='result_x'])
		public String fourthResultName() {
			element = driver.findElement(By.xpath(".//*[@id='result_3']/div/div[3]/div[1]/a/h2"));
			text1 = element.getText();
			return text1;
		}
		
		// Returning 5th result name (DYNAMIC - [@id='result_x'])
		public String fifthResultName() {
			element = driver.findElement(By.xpath(".//*[@id='result_4']/div/div[3]/div[1]/a/h2"));
			text1 = element.getText();
			return text1;
		}
	
		// Return 1st Result Price and account for non Amazon vendor price links
		public double firstPrice() {
		boolean check = isPresent("xpath", ".//*[@id='result_0']/div/div[4]/div[1]/a/span/span");
		double firstResultPrice = 0.0;
		if (check) {
			String priceText1 = driver.findElement(By.xpath(".//*[@id='result_0']/div/div[4]/div[1]/a/span/span/span")).getText();
			String priceText2 = driver.findElement(By.xpath(".//*[@id='result_0']/div/div[4]/div[1]/a/span/span/sup[2]")).getText();
			firstResultPrice = returnPrice(priceText1, priceText2);
			
		}else {
			firstResultPrice = Double.parseDouble(driver.findElement(By.xpath(".//*[@id='result_0']/div/div[4]/div/a/span[2]")).getText().replace("$", ""));
		}	
		return firstResultPrice;
		}
		
		// Return 2nd result price and account for non Amazon vendor price links 
		public double secondPrice() {
			boolean check = isPresent("xpath", ".//*[@id='result_1']/div/div[4]/div[1]/a/span/span");
			double secondResultPrice = 0.0;
			if (check) {
				String priceText1 = driver.findElement(By.xpath(".//*[@id='result_1']/div/div[4]/div[1]/a/span/span/span")).getText();
				String priceText2 = driver.findElement(By.xpath(".//*[@id='result_1']/div/div[4]/div[1]/a/span/span/sup[2]")).getText();
				secondResultPrice = returnPrice(priceText1, priceText2);
			}else {
				secondResultPrice = Double.parseDouble(driver.findElement(By.xpath(".//*[@id='result_1']/div/div[4]/div/a/span[2]")).getText().replace("$", ""));
			}
			return secondResultPrice;
		}
		
		// Return 3rd result price and account for non Amazon vendor price links 
		public double thirdPrice() {
			boolean check = isPresent("xpath", ".//*[@id='result_2']/div/div[4]/div[1]/a/span/span");
			double thirdResultPrice = 0.0;
			if (check) {
				String priceText1 = driver.findElement(By.xpath(".//*[@id='result_2']/div/div[4]/div[1]/a/span/span/span")).getText();
				String priceText2 = driver.findElement(By.xpath(".//*[@id='result_2']/div/div[4]/div[1]/a/span/span/sup[2]")).getText();
				thirdResultPrice = returnPrice(priceText1, priceText2);
			}else {
				thirdResultPrice = Double.parseDouble(driver.findElement(By.xpath(".//*[@id='result_2']/div/div[4]/div/a/span[2]")).getText().replace("$", ""));	
			}
			return thirdResultPrice;
		}
		
		
		// Return 1st result review stars and deal with products with no reviews 
		public double firstReviews(){
			boolean check = isPresent("xpath", ".//*[@id='result_0']//span[@class='a-icon-alt'][contains(.,'stars')]");
			double firstResultStars = 0.0;
			if (check) {
				firstResultStars = Double.parseDouble(driver.findElement(By.xpath(".//*[@id='result_0']//span[@class='a-icon-alt'][contains(.,'stars')]")).getAttribute
						("textContent").substring(0, 3));
			} else {
				System.out.println("Result 1 has no reviews");
			}
			return firstResultStars;
		}
		
		// Return 2nd result review stars and deal with products with no reviews 
		public double secondReviews() {
			boolean check = isPresent("xpath", ".//*[@id='result_1']//span[@class='a-icon-alt'][contains(.,'stars')]");
			double secondResultStars = 0.0;
			if (check) {
				secondResultStars = Double.parseDouble(driver.findElement(By.xpath(".//*[@id='result_1']//span[@class='a-icon-alt'][contains(.,'stars')]")).getAttribute
						("textContent").substring(0, 3));
			} else {
				System.out.println("Result 2 has no reviews");
			}
			return secondResultStars;
		}
		
		public double thirdReviews() {
			boolean check = isPresent("xpath", ".//*[@id='result_2']//span[@class='a-icon-alt'][contains(.,'stars')]");
			double thirdResultStars = 0.0;
			if (check) {
				thirdResultStars = Double.parseDouble(driver.findElement(By.xpath(".//*[@id='result_2']//span[@class='a-icon-alt'][contains(.,'stars')]")).getAttribute
						("textContent").substring(0, 3));
			} else {
				System.out.println("Result 3 has no reviews");
			}
			return thirdResultStars;
			
		}
	
}	


