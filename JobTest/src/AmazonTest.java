package JobTest;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class AmazonTest {

	private WebDriver driver;
	private String url = "https://www.amazon.com";
	PageObject po;
	OperationMethods om;
	

	
	@BeforeTest
	public void beforeTest() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\mikej\\Desktop\\Eclipse\\chromedriver.exe");
		driver = new ChromeDriver();
		po = new PageObject(driver);
		om = new OperationMethods(driver);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	
	@Test
	public void test() {
		
		po.searchForItem("ipad air 2 case");
		po.clickPlastic();
		po.refineBy$("20", "100");
		
		ArrayList<Products> items = new ArrayList<Products>();
		items.add(new Products(om.firstResultName(), om.firstPrice(), om.firstReviews()));
		items.add(new Products(om.secondResultname(), om.secondPrice(), om.secondReviews()));
		items.add(new Products(om.thridResultname(), om.thirdPrice(), om.thirdReviews()));
		
		System.out.println("Displaying Name, Price, and Raiting for products");
		for (Products i : items)
			System.out.println(i);
		
		ArrayList<Double> priceList = new ArrayList<Double>();
		priceList.add(om.firstPrice());
		priceList.add(om.secondPrice());
		priceList.add(om.thirdPrice());
		
		System.out.println("Asserting all prices are greater than 20 and less than 100");
		for (int i = 0; i < priceList.size(); i++) {
			Assert.assertTrue(priceList.get(i) >= 20.00);
			Assert.assertTrue(priceList.get(i) <=100.00);
			System.out.println(priceList.get(i));	
		}
		
		class ProductsPriceComparator implements Comparator<Products> {
			@Override
			public int compare(Products prod1, Products prod2) {
				return Double.compare(prod1.getPrice(), - prod2.getPrice());
			}
		}
		
		Collections.sort(items, new ProductsPriceComparator());
		
		System.out.println("***** Sorting by Price ******");
		for (Products prod : items) {
			System.out.println(prod);
		}
		
		class ProductsReviewsComparator implements Comparator<Products> {
			@Override
			public int compare(Products prod1, Products prod2) {
				return Double.compare(prod1.getReviews(), prod2.getReviews());
			}
		}
		
		Collections.sort(items, new ProductsReviewsComparator().reversed());
		
		System.out.println("**** Sorting by Reviews *****");
		
		for (Products prod2 : items) {
			System.out.println(prod2);
		}
		
	}


}
